function BookStore(name) {
    this.name = name;
    this.books = [];
    this.shoppingCard = [];
    this.wishlist = [];

    this.addBook = function(book) {
        this.books.push(book);
    }

    this.listBooks = function(element) {
        let html = '';
        let index = 0;
        for(let i=0; i<this.books.length; i++) {
            if(this.books[i].quantity == 0) {
                html += 
            `<li class="card" data-index="${index}">
                <h2>${this.books[i].title}<h2>
                <img src="${this.books[i].image}">
                <h4>${this.books[i].description}</h4>
                <p>Price: ${this.books[i].price}</p>
                <p>Quantity: out of stroke</p>
            </li>`

            } else {
                html += 
            `<li class="card" data-index="${index}">
                <h2>${this.books[i].title}<h2>
                <img src="${this.books[i].image}">
                <h4>${this.books[i].description}</h4>
                <p>Price: ${this.books[i].price}</p>
                <p>Quantity: ${this.books[i].quantity}</p>
                <button class="buy">Buy</button>
                <button class="add-to-whishlist">Add to whishlist</button>
            </li>`           
            }
            index++
        }
        element.innerHTML = html;
    }

    this.addToshoppingCart = function(book1) {
        this.shoppingCard.push(book1)
        this.listToShoppingCart();
    }

    this.listToShoppingCart = function() {
        let element = document.getElementById('shopping-cart');
        let html = "";
        let index = 0;
        for(let i=0; i<this.shoppingCard.length; i++) {
            html += `<li data-index=${index}> ${this.shoppingCard[i].title} ${this.shoppingCard[i].author} ${this.shoppingCard[i].price}
            <button class="delete-shop">remove</button>
            </li>
            `
            index++;
        }
        element.innerHTML = html;
    }


    this.addToWhishlist = function(book) {
        this.wishlist.push(book)
        this.listToWhishList();
    }

    this.listToWhishList = function() {
        let element = document.getElementById('wishlist');
        let html = "";
        let index = 0;
        for(let i=0; i<this.wishlist.length; i++) {
            html += `<li data-index="${index}"> ${this.wishlist[i].title} ${this.wishlist[i].author} ${this.wishlist[i].price}
            <button class="delete-wish">remove</button></li>
            `
            index++;
        }
        element.innerHTML = html;
    }
    
}


function Book(title, author, image, price, quantity, description) {
    this.title = title;
    this.author = author;
    this.image = image;
    this.price = price;
    this.quantity = quantity;
    this.description = description;
}


let myBookStore = new BookStore('Filip');

let addBtn = document.getElementById('add-btn');
addBtn.addEventListener('click', function(e) {
    e.preventDefault()
    let title = document.getElementById('title').value;
    let author = document.getElementById('author').value;
    let image = document.getElementById('thumbnail').value;
    let price = document.getElementById('price').value;
    let quantity = document.getElementById('quantity').value;
    let description = document.getElementById('description').value;

    let book = new Book(title, author, image, price, quantity, description);

    myBookStore.addBook(book);
    let shoppingCart = document.getElementById('books-list');

    myBookStore.listBooks(shoppingCart);
})


document.addEventListener('click', function(e) {
    if(e.target.classList.contains('buy')) {
        let bookIndex = parseInt(e.target.closest('li').getAttribute('data-index'));
        let book1 = myBookStore.books[bookIndex];
        myBookStore.addToshoppingCart(book1);

        book1.quantity = book1.quantity - 1;
        let shoppingCart = document.getElementById('books-list')
        myBookStore.listBooks(shoppingCart);
        
    }
})


document.addEventListener('click', function(e) {
    if(e.target.classList.contains('add-to-whishlist')) {
        let bookIndex = parseInt(e.target.closest('li').getAttribute('data-index'))
        let book2 = myBookStore.books[bookIndex]
        myBookStore.addToWhishlist(book2)
    }
})

document.addEventListener('click', function(e) {
    if(e.target.classList.contains('delete-shop')) {
        let indexLi = parseInt(e.target.closest('li').getAttribute('data-index'));
        let info = myBookStore.shoppingCard[indexLi];
        console.log(info)
        let position = myBookStore.shoppingCard.indexOf(info);
        myBookStore.shoppingCard.splice(position, 1);
        console.log(myBookStore.shoppingCard);
        myBookStore.listToShoppingCart();
    }
})

document.addEventListener('click', function(e) {
    if(e.target.classList.contains('delete-wish')) {
        let index1 = parseInt(e.target.closest('li').getAttribute('data-index'))
        console.log(index1)
        let info1 = myBookStore.wishlist[index1];
        let position = myBookStore.wishlist.indexOf(info1);
        myBookStore.wishlist.splice(position, 1);
        console.log(myBookStore.wishlist);
        myBookStore.listToWhishList();
    }
})

