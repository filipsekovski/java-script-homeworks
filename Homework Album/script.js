//For this exercise you need to create an Album list like the one in the image. You should write two constructor functions. The first one is MusicAlbum with the following properties (name, artist, artistImage, songs, yearReleased, albumCover, albumDuration) and two methods (addSong) to add the songs to your album and the second method (listAllSongs) to display all songs on the screen just like in the image.

//The second constructor function is Song with the following properties (title, artist, plays, duration).

//After that create the Album and create a couple of Songs. Add the Songs to the Album and list all songs on the screen.

//Use the image from the Spotify album as a reference for the structure, but keep in mind that you can use an album and an artist of your choice if you like.

//Bonus(but not required): Style the HTML just like in the image.

let album_Section = document.querySelector('.album-section');
let play_Section = document.querySelector('.play-section');
let title_Section = document.querySelector('.title-section');
let songs_Section = document.querySelector('.songs-section');

function MusicAlbum(name, artist, yearReleased, songs, albumDuration) {
    this.name = name;
    this.artist = artist;
    this.songs = songs;
    this.yearReleased = yearReleased;
    this.albumDuration = albumDuration;

    this.allsongs = [];

    this.addSong = function(song) {
        this.allsongs.push(song)
    }

    this.listAllSongs = function(firstSection,secondSecion,thirdSection,forthSection) {
        let header = 
        `
            <header>
                <div class="album-container">
                    <img src="album_img.jpg" class="album-img">
                    <div class="info-album">
                        <p class="first-p">Album</p>
                        <h1>${this.name}</h1>

                        <div class="last-info">
                            <img src="ed_profile.jpg" class="artist-img">
                            <p><b>${this.artist}</b> + ${this.yearReleased} + ${this.songs}, ${this.albumDuration}</p>
                        </div>
                    </div>
                </div>
            </header> 
        `
        firstSection.innerHTML = header;

        let playSection = 
        `
            <div class="playSection">
                <div class="left">
                    <button class="play-button"> <i class="fa-solid fa-play"></i></button>

                    <div class="icons">
                        <i class="fa-solid fa-arrow-right-long"></i>
                        <i class="fa-solid fa-plus border"></i>
                        <i class="fa-solid fa-arrow-down border"></i>
                        <i class="fa-solid fa-ellipsis"></i>
                    </div>

                </div>

                <div class="right">
                    <p>List</p>
                    <i class="fa-solid fa-bars"></i>
                </div>
            </div>
        `
        secondSecion.innerHTML = playSection;

        let infoSongsProperties = 
        `
            <div class="infoSongsProperties">
                <div class="d-flex">
                    <div class="title">
                        <p>#</p>
                        <p>Title</p>
                    </div>

                    <p>Plays</p>
            
                    <i class="fa-regular fa-clock"></i>
                </div> 
                <hr>
            </div>
            

        `
        thirdSection.innerHTML = infoSongsProperties;

        for(let i=0; i<this.allsongs.length; i++) {
            let newDiv = 
            `
                <div class="allsongsEd">
                    <div class="song-artist">
                        <p>${this.allsongs[i].title}</p>
                        <p class="artist">${this.allsongs[i].artist}</p>
                    </div>
                    
                    <p class="views">${this.allsongs[i].plays}</p>

                    <p>${this.allsongs[i].duration}</p>
                </div>
            `
            forthSection.innerHTML += newDiv;
        }
        
    }
}

function Song(title, artist, plays, duration) {
    this.title = title;
    this.artist = artist;
    this.plays = plays;
    this.duration = duration;
}

let One = new Song("One", "Ed Sheeran", "270,238,109", "4:12");
let Mess = new Song("I'm a Mess", "Ed Sheeran", "330,037,221", "4:04");
let Sing = new Song("Sing", "Ed Sheeran", "547,350,732", "3:55");
let Dont = new Song("Don't", "Ed Sheeran", "745,244,385", "3:39");
let Nina = new Song("Nina", "Ed Sheeran", "142,478,996", "3:45");
let Photograph = new Song("Photograph", "Ed Sheeran", "2,450,452,070", "4:18");
let Bloodstream = new Song("Bloodstream", "Ed Sheeran", "308,675,242", "5:00");

let Album = new MusicAlbum("x (Deluxe Edition)", "Ed Sheeran", 2014, "16 songs", "1 hr 5 min");

Album.addSong(One);
Album.addSong(Mess);
Album.addSong(Sing);
Album.addSong(Dont);
Album.addSong(Nina);
Album.addSong(Photograph);
Album.addSong(Bloodstream);

Album.listAllSongs(album_Section,play_Section,title_Section,songs_Section)
console.log(Album.allsongs)






